CFLAGS=-Wall -Wextra -Wconversion -std=c99 -pedantic -ggdb
SRC=src/main.c src/game.c src/player.c src/map.c src/entity.c src/load.c src/ray.c
LIBS=-lraylib -lm

all:
	$(CC) $(CFLAGS) $(SRC) -o game $(LIBS)
edit:
	$(CC) $(CFLAGS) src/editor.c -o editor $(LIBS)
clean:
	$(RM) game
	$(RM) editor
