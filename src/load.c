#include "load.h"

void load_read_data(GameData *game_data) {
	FILE *data = fopen("data.conf", "r");
	if (!data) {
		printf("Error opening data.conf for reading.\n");
		exit(1);
	}

	char buffer[BUFFER_SIZE];

	// read data in order of data.conf
	fgets(buffer, BUFFER_SIZE, data);
	game_data->map_dimension = atoi(buffer);
	printf("LOAD: GameData map_dimension read success\n");
	fgets(buffer, BUFFER_SIZE, data);
	game_data->map_block_size = atoi(buffer);
	printf("LOAD: GameData map_block_size read success\n");

	game_data->map_element_count = game_data->map_dimension * game_data->map_dimension;
	printf("LOAD: GameData map_element_count read success\n");

	// allocate memory for array using dim and write buffer to it
	game_data->map_array = malloc((size_t)BUFFER_SIZE);
	if (!game_data->map_array) {
		printf("Error allocating memory GameData map_array\n");
		exit(1);
	}

	// read and remove new line from the buffer
	fgets(buffer, BUFFER_SIZE, data);
	buffer[strcspn(buffer, "\n")] = '\0';
	strcpy(game_data->map_array, buffer);
	printf("LOAD: GameData map_array read success\n");

	// allocate memory for block array using dim
	game_data->map_block_array = malloc(sizeof(Block) * (size_t)game_data->map_element_count);
	if (!game_data->map_block_array) {
		printf("Error allocating memory GameData map_block_array\n");
		exit(1);
	}
	fclose(data);
}
