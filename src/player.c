#include "player.h"

Player player_init() {
	Player p;
	p.angle = 0.0f;
	p.vertical_offset = 0.0f;
	p.vertical_sensitivity = 0.30f;
	p.position = Vector2Zero();
	p.direction = Vector2Zero();
	p.move_speed = 1;
	p.turn_speed = 3;
	p.radius = 5;
	return p;
}

void player_movement(Player *player) {
	// TODO: Normalize turn speed based on GetMouseDelta()
	if (GetMouseDelta().x > 0) {
		// printf("Mouse moving right\n");
		player->angle += player->turn_speed;
		if (player->angle > 359) player->angle -= 360;
		if (player->angle < 0) player->angle += 360;
	}
	if (GetMouseDelta().x < 0) {
		// printf("Mouse moving left\n");
		player->angle -= player->turn_speed;
		if (player->angle > 359) player->angle -= 360;
		if (player->angle < 0) player->angle += 360;
	}

	// if (IsKeyDown(KEY_A)) {
	// 	player->angle -= player->turn_speed;
	// 	if (player->angle < 0) {
	// 		player->angle += 360;
	// 	}
	// }
	// if (IsKeyDown(KEY_D)) {
	// 	player->angle += player->turn_speed;
	// 	if (player->angle >= 360) {
	// 		player->angle -= 360;
	// 	}
	// }

	const float dx = player->move_speed * cosf(player->angle * DEG2RAD);
	const float dy = player->move_speed * sinf(player->angle * DEG2RAD);

	if (IsKeyDown(KEY_W)) {
		player->position.x += dx;
		player->position.y += dy;
	}
	if (IsKeyDown(KEY_S)) {
		player->position.x -= dx;
		player->position.y -= dy;
	}
}

void player_update(Player *player) {
	player_movement(player);
}

void player_draw(Player player) {
	Vector2 direction = {20 * cosf(player.angle * DEG2RAD) + player.position.x, 20 * sinf(player.angle * DEG2RAD) + player.position.y};
	DrawCircleV(player.position, player.radius, YELLOW);
	DrawLineV(player.position, direction, GREEN);
}
