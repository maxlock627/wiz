#ifndef _MAP_H_
#define _MAP_H_

#include <stdio.h>
#include <raylib.h>

#include "game.h"
#include "entity.h"

#define MAP_BLOCK_SIZE 16

// Read the map array and populate map with block and type
void map_init(GameData *game_data, Player *player);
void map_draw(GameData game_data, Player player);

#endif // _MAP_H_
