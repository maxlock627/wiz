#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <raylib.h>
#include <raymath.h>

typedef struct {
	Vector2 position;
	Vector2 direction;
	float vertical_offset;
	float vertical_sensitivity;
	float angle;
	float radius;
	float move_speed;
	float turn_speed;
} Player;

// Initialize player data with zeroes
Player player_init();
// Update player position and camera target
void player_movement(Player *player);
// Update player data
void player_update(Player *player);
// Draw the player arm and animations
void player_draw(Player player);

#endif // _PLAYER_H_
