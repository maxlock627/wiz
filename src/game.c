#include "game.h"
#include "map.h"
#include "load.h"
#include "player.h"
#include "ray.h"
#include <raylib.h>
#include <stdio.h>

GameData game_data_init() {
	GameData gd;
	gd.map_toggle = false;
	gd.map_array = NULL;
	gd.map_block_array = NULL;
	gd.map_block_size = 0;
	gd.map_dimension = 0;
	gd.map_element_count = 0;
	gd.map_draw_offset = Vector2Zero();
	return gd;
}

void game_init(GameData *game_data, Player *player) {
	load_read_data(game_data);
	map_init(game_data, player);
	DisableCursor();
}

void game_update_keys(GameData *game_data) {
	if (IsKeyPressed(KEY_TAB)) {
		if (game_data->map_toggle) game_data->map_toggle = false;
		else if (!game_data->map_toggle) game_data->map_toggle = true;
	}
}

void game_update(GameData *game_data, Player *player) {
	// Debug info for player
	char buffera[20];
	char buffery[20];
	char bufferx[20];
	int lenx = sprintf(bufferx, "%f", player->position.x);
	if (lenx == -1) printf("Error converting float %f\n", player->position.x);
	int leny = sprintf(buffery, "%f", player->position.y);
	if (leny == -1) printf("Error converting float %f\n", player->position.y);
	int lena = sprintf(buffera, "%f", player->angle);
	if (lena == -1) printf("Error converting float %f\n", player->angle);

	// Get the offset in case the screen dimensions change
	game_data->map_draw_offset.x = ((float)GetScreenWidth() - ((float)game_data->map_dimension * (float)game_data->map_block_size)) / 2.0f;
	game_data->map_draw_offset.y = ((float)GetScreenHeight() - ((float)game_data->map_dimension * (float)game_data->map_block_size)) / 2.0f;

	// Non gameplay keys
	game_update_keys(game_data);

	// Stop everything in the game if map is toggled
	if (!game_data->map_toggle) {
		player_update(player);
	}

	BeginDrawing();
	{
		ClearBackground(BLACK);

		// Crosshair pixel
		DrawCircle(GetScreenWidth()/2, GetScreenHeight()/2, 3, GREEN);
		ray_draw(*game_data, *player);
		if (game_data->map_toggle) map_draw(*game_data, *player);

		DrawFPS(0, 0);
		DrawText(bufferx, 0, 21, 21, DARKBLUE);
		DrawText(buffery, 0, 42, 21, DARKBLUE);
		DrawText(buffera, 0, 63, 21, DARKBLUE);
	}
	EndDrawing();
}

void game_free(GameData *game_data) {
	if (game_data->map_array) free(game_data->map_array);
	if (game_data->map_block_array) free(game_data->map_block_array);
	game_data->map_dimension = 0;
	game_data->map_element_count = 0;

	printf("GAME: GameData free\n");
}
