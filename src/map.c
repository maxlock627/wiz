#include "map.h"
#include "entity.h"
#include "player.h"

void map_init(GameData *game_data, Player *player) {
	int index = 0;
	for (int i = 0; i < game_data->map_dimension; i++) {
		for (int j = 0; j < game_data->map_dimension; j++) {
			if (game_data->map_array[i * game_data->map_dimension + j] == '0') {
				game_data->map_block_array[index] =
				(Block){(Rectangle){
					(float)j * (float)game_data->map_block_size,
					(float)i * (float)game_data->map_block_size,
					(float)game_data->map_block_size,
					(float)game_data->map_block_size},
					NONE};
			}
			if (game_data->map_array[i * game_data->map_dimension + j] == '1') {
				game_data->map_block_array[index] =
				(Block){(Rectangle){
					(float)j * (float)game_data->map_block_size,
					(float)i * (float)game_data->map_block_size,
					(float)game_data->map_block_size,
					(float)game_data->map_block_size},
					WALL};
			}
			// Set player position and make the block type none
			if (game_data->map_array[i * game_data->map_dimension + j] == 'p') {
				player->position.x = (float)j * (float)game_data->map_block_size;
				player->position.y = (float)i * (float)game_data->map_block_size;

				game_data->map_block_array[index] =
				(Block){(Rectangle){
					(float)j * (float)game_data->map_block_size,
					(float)i * (float)game_data->map_block_size,
					(float)game_data->map_block_size,
					(float)game_data->map_block_size},
					NONE};
			}
			index++;
		}
	}
}

void map_draw(GameData game_data, Player player) {
	for (int i = 0; i < game_data.map_element_count; i++) {
		switch (game_data.map_block_array[i].type) {
			case NONE:
				DrawRectangleRec((Rectangle){
					game_data.map_block_array[i].body.x+game_data.map_draw_offset.x,
					game_data.map_block_array[i].body.y+game_data.map_draw_offset.y,
					game_data.map_block_array[i].body.width,
					game_data.map_block_array[i].body.height},
					BLACK);
				// DrawRectangleRec(game_data.map_block_array[i].body, BLACK);
				break;
			case WALL:
				DrawRectangleRec((Rectangle){
					game_data.map_block_array[i].body.x+game_data.map_draw_offset.x,
					game_data.map_block_array[i].body.y+game_data.map_draw_offset.y,
					game_data.map_block_array[i].body.width,
					game_data.map_block_array[i].body.height},
					WHITE);
				// DrawRectangleRec(game_data.map_block_array[i].body, WHITE);
				break;
			default:
				DrawRectangleRec((Rectangle){
					game_data.map_block_array[i].body.x+game_data.map_draw_offset.x,
					game_data.map_block_array[i].body.y+game_data.map_draw_offset.y,
					game_data.map_block_array[i].body.width,
					game_data.map_block_array[i].body.height},
					PINK);
				break;
		}
	}

	// Draw player over map
	Vector2 player_position_offset = {player.position.x + game_data.map_draw_offset.x, player.position.y + game_data.map_draw_offset.y};
	Vector2 direction = {20 * cosf(player.angle * DEG2RAD) + player_position_offset.x, 20 * sinf(player.angle * DEG2RAD) + player_position_offset.y};
	DrawCircleV(player_position_offset, player.radius, YELLOW);
	DrawLineV(player_position_offset, direction, GREEN);

	// player_draw(player);
}
