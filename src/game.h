#ifndef _GAME_H_
#define _GAME_H_

#include <raylib.h>
#include <stdio.h>

#include "entity.h"
#include "player.h"

#define INIT_SCREEN_WIDTH 1280
#define INIT_SCREEN_HEIGHT 720
#define MIN_SCREEN_WIDTH 400
#define MIN_SCREEN_HEIGHT 400

typedef struct {
	bool map_toggle;
	int map_dimension;
	int map_element_count;
	Vector2 map_draw_offset;
	char *map_array;
	Block *map_block_array;
	int map_block_size;
} GameData;

GameData game_data_init();
void game_init(GameData *game_data, Player *player);
void game_update(GameData *game_data, Player *player);
void game_update_keys(GameData *game_data);
void game_free(GameData *game_data);

#endif // _GAME_H_
