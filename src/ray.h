#ifndef _RAYCASTER_H_
#define _RAYCASTER_H_

#include <raylib.h>
#include <raymath.h>

#include "game.h"
#include "player.h"
#include "map.h"

#define RAY_COUNT 240.0f
#define RAY_INCREMENT 60.0f/RAY_COUNT

void ray_draw(GameData, Player);

#endif // _RAYCASTER_H_
