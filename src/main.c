#include <raylib.h>
#include <stdio.h>

#include "game.h"
#include "player.h"

int main() {
	SetConfigFlags(FLAG_WINDOW_RESIZABLE);
	InitWindow(INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT, "test");
	SetTargetFPS(60);

	GameData game_data = game_data_init();
	Player player = player_init();
	game_init(&game_data, &player);

	while (!WindowShouldClose()) {
		game_update(&game_data, &player);
	}

	CloseWindow();
	game_free(&game_data);
	return 0;
}
