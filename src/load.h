#ifndef _LOAD_H_
#define _LOAD_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFFER_SIZE 10240

#include "game.h"

// Read data from data.conf and store into a GameData struct
void load_read_data(GameData *game_data);

#endif // _LOAD_H_
