#include <raylib.h>
#include <stdio.h>

#define MAP_SIZE 16
#define DIM 45

typedef struct {
	Rectangle body;
	int val;
} Blocks;

int main() {
	InitWindow(1280, 720, "array");
	SetTargetFPS(60);
	int start = (1280 - (45 * 16))/2;
	Blocks blocks[DIM*DIM];
	for (int i = 0; i < DIM; i++) {
		for (int j = 0; j < DIM; j++) {
				blocks[i * DIM + j] = (Blocks){(Rectangle){j*MAP_SIZE+start, i*MAP_SIZE, MAP_SIZE, MAP_SIZE}, 0};
			}
	}

	while (!WindowShouldClose()) {
		BeginDrawing();
		ClearBackground(BLACK);
		for (int i = 0; i < DIM*DIM; i++) {
			if (CheckCollisionPointRec(GetMousePosition(), blocks[i].body)) {
				if (IsMouseButtonDown(MOUSE_BUTTON_LEFT)) {
					blocks[i].val = 1;
				}
			}
		}

		for (int i = 0; i < DIM*DIM; i++) {
			if (blocks[i].val == 0)
				DrawRectangle(blocks[i].body.x, blocks[i].body.y, blocks[i].body.width, blocks[i].body.height, BLACK);
			if (blocks[i].val == 1)
				DrawRectangle(blocks[i].body.x, blocks[i].body.y, blocks[i].body.width, blocks[i].body.height, WHITE);
		}

		int x = (1280 - (45 * 16))/2;
		int y = 0;
		for (int i = 0; i <= DIM; i++) {
			DrawLine(start, y, start+DIM*MAP_SIZE, y, LIGHTGRAY);
			y += MAP_SIZE;
			DrawLine(x, 0, x, DIM*MAP_SIZE, LIGHTGRAY);
			x += MAP_SIZE;
		}

		EndDrawing();
	}

	for (int i = 0; i < DIM*DIM; i++) {
		printf("%d", blocks[i].val);
	}
	printf("\n");
	CloseWindow();
	return 0;
}
