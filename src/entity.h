#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <raylib.h>

typedef enum {NONE, WALL, DOOR, P_ENTER, P_EXIT} Type;

typedef struct{
	Rectangle body;
	Type type;
} Block;

#endif // _ENTITY_H_
