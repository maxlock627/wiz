#include "ray.h"

void ray_draw(GameData game_data, Player player) {
	float rx, ry, ra, ox, oy, dist;
	int depth;

	float ray_scale = (float)GetScreenWidth() / RAY_COUNT;
	float inc = 60.0f/RAY_COUNT;

	ra = player.angle - 30;
	if (ra > 360) ra -= 360;
	if (ra < 0) ra += 360;

	for (int i = 0; i <= RAY_COUNT; i++) {
		float hx, hy;
		float dist_h = 10000;
		float _atan = -1/tanf(ra * DEG2RAD);
		depth = 0;
		// facing up
		if (ra > 180) {
			ry = (float)(((int)player.position.y>>(int)log2(MAP_BLOCK_SIZE))<<(int)log2(MAP_BLOCK_SIZE));
			rx = (player.position.y-ry)*_atan+player.position.x;

			oy = -MAP_BLOCK_SIZE;
			ox = -oy*_atan;
		}
		// facing down
		if (ra < 180) {
			ry = (float)(((int)player.position.y>>(int)log2(MAP_BLOCK_SIZE))<<(int)log2(MAP_BLOCK_SIZE)) + MAP_BLOCK_SIZE;
			rx = (player.position.y-ry)*_atan+player.position.x;

			oy = MAP_BLOCK_SIZE;
			ox = -oy*_atan;
		}
		// facing left or right
		if (ra == 0 || ra == 360 || ra == 180) {
			rx = player.position.x;
			ry = player.position.y;
			depth = 45;
		}

		// calculating horizontal distance
		while (depth < 45) {
			int x = (int)rx/MAP_BLOCK_SIZE;
			int y = (int)ry/MAP_BLOCK_SIZE;
			if (x < 0 || x > game_data.map_dimension || game_data.map_array[y*game_data.map_dimension+x] == '1' || game_data.map_array[(y-1)*game_data.map_dimension+x] == '1') {
				hx = rx;
				hy = ry;
				dist_h = Vector2Distance(player.position, (Vector2){hx, hy});
				depth = 45;
			}
			else {
				rx += ox;
				ry += oy;
				depth++;
			}
		}
		// DrawLineEx((Vector2){player.position.x-1, player.position.y-1}, (Vector2){rx-1, ry-1}, 3, GREEN);

		float vx, vy;
		float dist_v = 10000;
		float _ntan = -tanf(ra * DEG2RAD);
		depth = 0;

		// looking left
		if (ra > 90 && ra < 270) {
			rx = (float)(((int)player.position.x>>(int)log2(MAP_BLOCK_SIZE))<<(int)log2(MAP_BLOCK_SIZE));
			ry = (player.position.x-rx)*_ntan+player.position.y;

			ox = -MAP_BLOCK_SIZE;
			oy = -ox * _ntan;
		}
		// looking right
		if (ra < 90 || ra > 270) {
			rx = (float)(((int)player.position.x>>(int)log2(MAP_BLOCK_SIZE))<<(int)log2(MAP_BLOCK_SIZE)) + MAP_BLOCK_SIZE;
			ry = (player.position.x-rx)*_ntan+player.position.y;

			ox = MAP_BLOCK_SIZE;
			oy = -ox*_ntan;
		}
		// looking straight up or down
		if (ra == 270 || ra == 90) {
			rx = player.position.x;
			ry = player.position.y;
			depth = 45;
		}

		while (depth < 45) {
			int x = (int)rx/MAP_BLOCK_SIZE;
			int y = (int)ry/MAP_BLOCK_SIZE;
			if (y < 0 || y > game_data.map_dimension || game_data.map_array[y*game_data.map_dimension+x] == '1' || game_data.map_array[y*game_data.map_dimension+(x-1)] == '1') {
				vx = rx;
				vy = ry;
				dist_v = Vector2Distance(player.position, (Vector2){vx, vy});
				depth = 45;
			}
			else {
				rx += ox;
				ry += oy;
				depth++;
			}
		}
		// DrawLineEx((Vector2){player.position.x+1, player.position.y+1}, (Vector2){rx+1, ry+1}, 3, RED);

		// determine which ray to use based on distance
		if (dist_h < dist_v) {
			rx = hx;
			ry = hy;
			dist =  dist_h;
		}
		else {
			rx = vx;
			ry = vy;
			dist = dist_v;
		}

		// DrawLineV(player.position, (Vector2){rx, ry}, ORANGE);

		// normalize to scale of 0.0 to 1.0
		float norm = (dist - 1) / (((float)game_data.map_dimension - 3)*MAP_BLOCK_SIZE);
		// sigmoid function, norm - (higher = slower)
		float alpha = 1 / (1 + expf(4.0f * (norm - 0.5f)));
		Color color = Fade(WHITE, alpha);
  //
		// fix fish eye effect
		float fish = player.angle-ra;
		if (fish <= 0) fish += 360;
		if (fish >= 360) fish -= 360;
		dist = dist * cosf(fish * DEG2RAD);
  //
		// generate lines
		float line_height = (MAP_BLOCK_SIZE*(float)GetScreenWidth())/dist;
		if (line_height > (float)GetScreenHeight()) line_height = (float)GetScreenHeight();
  //
		float line_offset = ((float)GetScreenHeight() - line_height)/2.0f;
  //
		// draw lines
		DrawLineEx((Vector2){(float)i*ray_scale, line_offset},
				   (Vector2){(float)i*ray_scale, line_height+line_offset},
				   ray_scale,
				   color);

		ra += inc;
		if (ra >= 360) ra -= 360;
		if (ra <= 0) ra += 360;
	}
}
